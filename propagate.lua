--[[
Propagate
1.0
--]]

local apow = require "apow"
local cocreate = coroutine.create
local corunning = coroutine.running
local coyield = coroutine.yield
local coresume = coroutine.resume
local costatus = coroutine.status
local error = error
local select = select

local dummy = {}
local ischecked = {}
setmetatable(ischecked, {__mode="k"})

-- Usage:
-- propagate.enable ^ function(...) end
local enable = apow.annotation ^ function(self, target)
  return function(...)
    local co = cocreate(target)
    local function recurse2(recurse, ok, ...)
      ischecked[co] = nil
      local status = costatus(co)
      if status == "dead" then
        if ok then
          return ...
        else
          error((...))
        end
      else
        if ok then
          if ... == dummy then
            return select(2, ...)
          end
          return recurse(coyield(...))
        else
          -- TODO handle this error condition
          error("Unexpected error condition")
        end
      end
    end
    local function recurse(...)
      if costatus(co) == "dead" then
        -- TODO handle this error condition
        error("Unexpected error condition")
      end
      ischecked[co] = true
      return recurse2(recurse, coresume(co, ...))
    end
    return recurse(...)
  end
end

local propagate = function(...)
  if ... then
    return ...
  else
    if not ischecked[corunning()] then error("Attempt to propagate in invalid context") end
    error(coyield(dummy, ...))
  end
end

return {propagate = propagate, enable = enable}